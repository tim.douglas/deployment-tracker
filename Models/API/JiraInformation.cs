namespace deployment_tracker.Models.API {
        public class JiraInformation {
            public string Url { get; set; }
            public string Status { get; set; }
        }
} 