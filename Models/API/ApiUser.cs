namespace deployment_tracker.Models.API {
    public class ApiUser {
        public string Username { get; set; }
        public string Name { get; set; }
    }
}