
namespace deployment_tracker.Services.Identity {    
    public class ApplicationRole
    {
            public string Id { get; set; }
            public string Name { get; set; }
    }
}