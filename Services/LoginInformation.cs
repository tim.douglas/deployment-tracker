namespace deployment_tracker.Services {
    public class LoginInformation {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}