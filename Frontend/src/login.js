import React from 'react';

import bootstrapToPage from './utils/page-bootstrapper';
import LoginForm from './account/login-form';

bootstrapToPage(<LoginForm />);