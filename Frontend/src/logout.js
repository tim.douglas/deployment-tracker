import React from 'react';

import bootstrapToPage from './utils/page-bootstrapper';
import LogoutScreen from './account/logout-screen';

bootstrapToPage(<LogoutScreen />);